package at.qe.sepm.skeleton.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.services.WorkPackageService;
import at.qe.sepm.skeleton.utils.TeamOverviewData;

import java.util.List;
import java.util.Map;

import org.junit.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class ProjectTeamServiceTest extends TestUtils{

    @Autowired
    ProjectTeamService projectTeamService;
    @Autowired
    UserService userService;
    @Autowired
    WorkPackageService workPackageService;
    
    @Test
    @WithMockUser(username = "user1", authorities = {"MODERATOR"})
    public void testFindProjectTeams4Moderator() {
    	User testUser = userService.loadUser("user1");
    	if(testUser == null){
    		Assert.fail("Testuser not found");
    	}
    	List<ProjectTeam> projectTeams = projectTeamService.loadAllProjects4Moderator(testUser);
    	Assert.assertTrue("Works in", projectTeams.contains(projectTeamService.loadProject("S.M.A.R.D")));
    }

//    @Test
//    @WithMockUser(username = "admin", authorities = {"MODERATOR"})
//    public void findSum4WorkPackage(){
//    	Assert.assertEquals(7.0, workPackageService.totalTimeForWorkPackage(workPackageService.loadPackage("UI")),0.01);
//    }
//    

    @Test
    @WithMockUser(username = "user1", authorities = {"MODERATOR", "STUDENT"})
    public void testOverview4ProjectTeam() {
    	ProjectTeam team = projectTeamService.loadProject("S.M.A.R.D");
    	Map<WorkPackage,TeamOverviewData> overview = projectTeamService.getOverview4ProjectTeam(team);
    	Assert.assertNotNull("Overview no result", overview);
    	Assert.assertTrue("Overview missed Workpackage UI", containsWpWithName("UI", overview));
    	Assert.assertTrue("Overview missed Workpackage Spring", containsWpWithName("Spring", overview));
    	Assert.assertTrue("Overview missed Workpackage Pokemon", containsWpWithName("Pokemon", overview));
    	Assert.assertTrue("Overview missed Workpackage JPA", containsWpWithName("JPA", overview));
    	WorkPackage jpa = workPackageService.loadPackageWithName("JPA");
    	Assert.assertEquals("Aggregation not correct", 19.0, overview.get(jpa).getTimeSum(),0.01);
    	Assert.assertEquals("UserCount not correct", 2, overview.get(jpa).getNoOfUsers());
    	Assert.assertEquals("Average not correct", 9.5, overview.get(jpa).getAverage(),0.01);
    	Map<User, Double> subSums = overview.get(jpa).getSubSums();
    	Assert.assertEquals("Subsum not correct", 9.0, subSums.get(userService.loadUser("admin")),0.01);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"MODERATOR"})
    public void testGetNumberOfMembers(){
    	ProjectTeam team = projectTeamService.loadProject("S.M.A.R.D");
    	Assert.assertEquals("Number of Members not correct", 2, projectTeamService.getNumberOfMembers(team));
    }

    @Test
    @WithMockUser(username = "user1", authorities = {"MODERATOR", "STUDENT"})
    public void testGetTotalTimesProject(){
    	Map<User,Double> userTimes = projectTeamService.getTotalTimesProject(projectTeamService.loadProject("S.M.A.R.D"));
    	Assert.assertEquals("Aggregation for User not correct", 26.0, userTimes.get(userService.loadUser("user3")),0.01);
    	Assert.assertEquals("Aggregation for User not correct", 15.0, userTimes.get(userService.loadUser("admin")),0.01);
    }
    
}
