package at.qe.sepm.skeleton.tests;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.services.WorkPackageService;

/**
 * Some very basic tests for {@link UserService}.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class UserServiceTest extends TestUtils {

    @Autowired
    UserService userService;
    
    @Autowired
    ProjectTeamService projectTeamService;

    @Autowired
    WorkPackageService workPackageService;

    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testDatainitialization() {
        Assert.assertEquals("Insufficient amount of users initialized for test data source", 6, userService.getAllUsers().size());
        for (User user : userService.getAllUsers()) {
            if ("admin".equals(user.getUsername())) {
                Assert.assertTrue("User \"admin\" does not have role ADMIN", user.getRoles().contains(UserRole.ADMIN));
                Assert.assertNotNull("User \"admin\" does not have a createUser defined", user.getCreateUser());
                Assert.assertNotNull("User \"admin\" does not have a createDate defined", user.getCreateDate());
                Assert.assertNull("User \"admin\" has a updateUser defined", user.getUpdateUser());
                Assert.assertNull("User \"admin\" has a updateDate defined", user.getUpdateDate());
            } else if ("user1".equals(user.getUsername())) {
                Assert.assertTrue("User \"user1\" does not have role MODERATOR", user.getRoles().contains(UserRole.MODERATOR));
                Assert.assertNotNull("User \"user1\" does not have a createUser defined", user.getCreateUser());
                Assert.assertNotNull("User \"user1\" does not have a createDate defined", user.getCreateDate());
                Assert.assertNull("User \"user1\" has a updateUser defined", user.getUpdateUser());
                Assert.assertNull("User \"user1\" has a updateDate defined", user.getUpdateDate());
            } else if ("user2".equals(user.getUsername())) {
                Assert.assertTrue("User \"user2\" does not have role STUDENT", user.getRoles().contains(UserRole.STUDENT));
                Assert.assertNotNull("User \"user2\" does not have a createUser defined", user.getCreateUser());
                Assert.assertNotNull("User \"user2\" does not have a createDate defined", user.getCreateDate());
                Assert.assertNull("User \"user2\" has a updateUser defined", user.getUpdateUser());
                Assert.assertNull("User \"user2\" has a updateDate defined", user.getUpdateDate());
            //Daniel commented this, because we have 4 users now...
            //} else {
                //Assert.fail("Unknown user \"" + user.getUsername() + "\" loaded from test data source via UserService.getAllUsers");
            }
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testDeleteUser() {
        User adminUser = userService.loadUser("admin");
        Assert.assertNotNull("Admin user could not be loaded from test data source", adminUser);
        User toBeDeletedUser = userService.loadUser("user2");
        Assert.assertNotNull("User2 could not be loaded from test data source", toBeDeletedUser);

        userService.deleteUser(toBeDeletedUser);

        Assert.assertEquals("No user has been deleted after calling UserService.deleteUser", 5, userService.getAllUsers().size());
        User deletedUser = userService.loadUser("user2");
        Assert.assertNull("Deleted User2 could still be loaded from test data source via UserService.loadUser", deletedUser);

        for (User remainingUser : userService.getAllUsers()) {
            Assert.assertNotEquals("Deleted User2 could still be loaded from test data source via UserService.getAllUsers", toBeDeletedUser.getUsername(), remainingUser.getUsername());
        }
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testUpdateUser() {
        User adminUser = userService.loadUser("admin");
        Assert.assertNotNull("Admin user could not be loaded from test data source", adminUser);
        User toBeSavedUser = userService.loadUser("user1");
        Assert.assertNotNull("User1 could not be loaded from test data source", toBeSavedUser);

        Assert.assertNull("User \"user1\" has a updateUser defined", toBeSavedUser.getUpdateUser());
        Assert.assertNull("User \"user1\" has a updateDate defined", toBeSavedUser.getUpdateDate());

        toBeSavedUser.setEmail("changed-email@whatever.wherever");
        userService.saveUser(toBeSavedUser);

        User freshlyLoadedUser = userService.loadUser("user1");
        Assert.assertNotNull("User1 could not be loaded from test data source after being saved", freshlyLoadedUser);
        Assert.assertNotNull("User \"user1\" does not have a updateUser defined after being saved", freshlyLoadedUser.getUpdateUser());
        Assert.assertEquals("User \"user1\" has wrong updateUser set", adminUser, freshlyLoadedUser.getUpdateUser());
        Assert.assertNotNull("User \"user1\" does not have a updateDate defined after being saved", freshlyLoadedUser.getUpdateDate());
        Assert.assertEquals("User \"user1\" does not have a the correct email attribute stored being saved", "changed-email@whatever.wherever", freshlyLoadedUser.getEmail());
    }

    @DirtiesContext
    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testCreateUser() {
        User adminUser = userService.loadUser("admin");
        Assert.assertNotNull("Admin user could not be loaded from test data source", adminUser);

        User toBeCreatedUser = new User();
        toBeCreatedUser.setUsername("newuser");
        toBeCreatedUser.setPassword("passwd");
        toBeCreatedUser.setEnabled(true);
        toBeCreatedUser.setFirstName("New");
        toBeCreatedUser.setLastName("User");
        toBeCreatedUser.setEmail("new-email@whatever.wherever");
        toBeCreatedUser.setPhone("+12 345 67890");
        toBeCreatedUser.setRoles(Sets.newSet(UserRole.STUDENT, UserRole.MODERATOR));
        userService.saveUser(toBeCreatedUser);

        User freshlyCreatedUser = userService.loadUser("newuser");
        Assert.assertNotNull("New user could not be loaded from test data source after being saved", freshlyCreatedUser);
        Assert.assertEquals("User \"newuser\" does not have a the correct username attribute stored being saved", "newuser", freshlyCreatedUser.getUsername());
        Assert.assertEquals("User \"newuser\" does not have a the correct password attribute stored being saved", "passwd", freshlyCreatedUser.getPassword());
        Assert.assertEquals("User \"newuser\" does not have a the correct firstName attribute stored being saved", "New", freshlyCreatedUser.getFirstName());
        Assert.assertEquals("User \"newuser\" does not have a the correct lastName attribute stored being saved", "User", freshlyCreatedUser.getLastName());
        Assert.assertEquals("User \"newuser\" does not have a the correct email attribute stored being saved", "new-email@whatever.wherever", freshlyCreatedUser.getEmail());
        Assert.assertEquals("User \"newuser\" does not have a the correct phone attribute stored being saved", "+12 345 67890", freshlyCreatedUser.getPhone());
        Assert.assertTrue("User \"newuser\" does not have role MODERATOR", freshlyCreatedUser.getRoles().contains(UserRole.MODERATOR));
        Assert.assertTrue("User \"newuser\" does not have role STUDENT", freshlyCreatedUser.getRoles().contains(UserRole.STUDENT));
        Assert.assertNotNull("User \"newuser\" does not have a createUser defined after being saved", freshlyCreatedUser.getCreateUser());
        Assert.assertEquals("User \"newuser\" has wrong createUser set", adminUser, freshlyCreatedUser.getCreateUser());
        Assert.assertNotNull("User \"newuser\" does not have a createDate defined after being saved", freshlyCreatedUser.getCreateDate());
    }

    @Test(expected = org.springframework.orm.jpa.JpaSystemException.class)
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testExceptionForEmptyUsername() {
        User adminUser = userService.loadUser("admin");
        Assert.assertNotNull("Admin user could not be loaded from test data source", adminUser);

        User toBeCreatedUser = new User();
        userService.saveUser(toBeCreatedUser);
    }

    @Test(expected = org.springframework.security.authentication.AuthenticationCredentialsNotFoundException.class)
    public void testUnauthenticateddLoadUsers() {
        for (User user : userService.getAllUsers()) {
            Assert.fail("Call to userService.getAllUsers should not work without proper authorization");
        }
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(username = "user", authorities = {"STUDENT"})
    public void testUnauthorizedLoadUsers() {
        for (User user : userService.getAllUsers()) {
            Assert.fail("Call to userService.getAllUsers should not work without proper authorization");
        }
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(username = "user1", authorities = {"STUDENT"})
    public void testUnauthorizedLoadUser() {
        User user = userService.loadUser("admin");
        Assert.fail("Call to userService.loadUser should not work without proper authorization for other users than the authenticated one");
    }

    @WithMockUser(username = "user1", authorities = {"STUDENT"})
    public void testAuthorizedLoadUser() {
        User user = userService.loadUser("user1");
        Assert.assertEquals("Call to userService.loadUser returned wrong user", "user1", user.getUsername());
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(username = "user1", authorities = {"STUDENT"})
    public void testUnauthorizedSaveUser() {
        User user = userService.loadUser("user1");
        Assert.assertEquals("Call to userService.loadUser returned wrong user", "user1", user.getUsername());
        userService.saveUser(user);
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(username = "user1", authorities = {"STUDENT"})
    public void testUnauthorizedDeleteUser() {
        User user = userService.loadUser("user1");
        Assert.assertEquals("Call to userService.loadUser returned wrong user", "user1", user.getUsername());
        userService.deleteUser(user);
    }


    @Test
    @WithMockUser(username = "admin", authorities = {"MODERATOR", "STUDENT"})
    public void testFindOverview4User(){
    	User testUser = userService.loadUser("admin");
    	Map<WorkPackage,Double> overview = userService.getOverview4User(testUser);
    	Assert.assertNotNull("UserOverview no Result", overview);
    	Assert.assertTrue("Overview missed Workpackage UI", containsWpWithName("UI", overview));
    	Assert.assertTrue("Overview missed Workpackage Spring", containsWpWithName("Spring", overview));
    	Assert.assertTrue("Overview missed Workpackage Pokemon", containsWpWithName("Pokemon", overview));
    	Assert.assertTrue("Overview missed Workpackage JPA", containsWpWithName("JPA", overview));
    	Assert.assertEquals("Aggregation not correct", 9.0, overview.get(workPackageService.loadPackageWithName("JPA")),0.01);
    }

    @Test(expected = org.springframework.security.access.AccessDeniedException.class)
    @WithMockUser(username = "user1", authorities = {"STUDENT"})
    public void testFindOverview4UserAuth1(){
    	User testUser = userService.loadUser("user2");
    	Map<WorkPackage,Double> overview = userService.getOverview4User(testUser);
    	Assert.assertNotNull("UserOverview no Result", overview);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"STUDENT"})
    public void testFindOverview4UserAuth2(){
    	User testUser = userService.loadUser("admin");
    	Map<WorkPackage,Double> overview = userService.getOverview4User(testUser);
    	Assert.assertNotNull("UserOverview no Result", overview);
    	Assert.assertTrue("Overview missed Workpackage UI", containsWpWithName("UI", overview));
    	Assert.assertTrue("Overview missed Workpackage Spring", containsWpWithName("Spring", overview));
    	Assert.assertTrue("Overview missed Workpackage Pokemon", containsWpWithName("Pokemon", overview));
    	Assert.assertTrue("Overview missed Workpackage JPA", containsWpWithName("JPA", overview));
    	Assert.assertEquals("Aggregation not correct", 9.0, overview.get(workPackageService.loadPackageWithName("JPA")),0.01);
    }
    
    @Test
    @WithMockUser(username = "user1", authorities = {"ADMIN", "MODERATOR"})
    public void testFindUser4ProjectTeam(){
    	List<User> users = userService.getUser4ProjectTeam(projectTeamService.loadProject("S.M.A.R.D"));
    	Assert.assertNotNull("User4ProjectTeam no Result", users);
    	Assert.assertTrue("Users missed admin", users.contains(userService.loadUser("admin")));
    	Assert.assertTrue("Users missed user3", users.contains(userService.loadUser("user3")));
    }
 
    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testGetDesign(){
    	User testUser = userService.loadUser("admin");
    	Assert.assertEquals("Admin Design is not 'DEFAUT'", userService.getUserDesign(testUser), "DEFAULT");
    	testUser = userService.loadUser("user3");
    	Assert.assertEquals("User3 Design is not set, so it should be 'DEFAULT'", userService.getUserDesign(testUser), "DEFAULT");
    }
    
    @Test
    @WithMockUser(username = "admin", authorities = {"ADMIN"})
    public void testSetDesign(){
    	User testUser = userService.loadUser("user1");
    	userService.setUserDesign(testUser, "CottonCandy");
    	testUser = userService.loadUser("user1"); //reload if it is really in the DB
    	Assert.assertEquals("User1 Design was not set to 'CottonCandy'", userService.getUserDesign(testUser), "CottonCandy");
    }
    
    @Test
    public void testAllDesigns(){
    	List<String> designs = userService.getAllDesigns();
    	Assert.assertTrue("'DEFAULT' is not in the list", designs.contains("DEFAULT"));
    }
}
