package at.qe.sepm.skeleton.tests;

import java.util.Map;

import at.qe.sepm.skeleton.model.WorkPackage;

public class TestUtils {

	public TestUtils() {
		super();
	}

	protected boolean containsWpWithName(String name, Map<WorkPackage,?> set) {
		for(WorkPackage wp : set.keySet()){
			if(wp.getWorkPackageName().equals(name)){
				return true;
			}
		}
		return false;
	}

}