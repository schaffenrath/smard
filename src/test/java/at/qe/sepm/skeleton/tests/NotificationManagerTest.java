package at.qe.sepm.skeleton.tests;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.email.EmailNotifier;
import at.qe.sepm.skeleton.email.NotificationJob;
import at.qe.sepm.skeleton.email.NotificationManager;
import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.services.UserService;

//@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class NotificationManagerTest 
{
    @Autowired
    ProjectTeamService projectTeamService;
    @Autowired
    UserService userService;
    
    @Test
    @WithMockUser(username = "user1", authorities = {"MODERATOR"})
    public void testingSimpleNotificationEmail() throws AddressException, MessagingException 
    {
    	EmailNotifier en = new EmailNotifier("smtp.gmail.com", "587", "mailsmard@gmail.com", "mausaus123");
    	en.SendMessage("alin.porcic@gmail.com", "mailsmard@gmail.com", "Test Mail", "Test Mail");
    }
}
