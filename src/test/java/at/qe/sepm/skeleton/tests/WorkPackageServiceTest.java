package at.qe.sepm.skeleton.tests;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.services.WorkPackageService;

import java.util.List;
import java.util.Map;

import org.junit.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class WorkPackageServiceTest {

    @Autowired
    WorkPackageService workPackageService;
    @Autowired
    UserService userService;
    
    @Test
    @WithMockUser(username = "admin", authorities = {"STUDENT"})
    public void testFindWorkPackages4UserTest() {
    	User testUser = userService.loadUser("admin");
    	if(testUser == null){
    		Assert.fail("Testuser not found");
    	}
    	List<WorkPackage> workPackages = workPackageService.getPackagesWorkedOn(testUser);
    	Assert.assertTrue("Works on", workPackages.contains(workPackageService.loadPackageWithName("UI")));
    	Assert.assertTrue("Works on", workPackages.contains(workPackageService.loadPackageWithName("Spring")));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"MODERATOR", "STUDENT"})
    public void testFindSum4WorkPackage(){
    	Assert.assertEquals(24.0, workPackageService.totalTimeForWorkPackage(workPackageService.loadPackageWithName("UI")),0.01);
    }
    
}
