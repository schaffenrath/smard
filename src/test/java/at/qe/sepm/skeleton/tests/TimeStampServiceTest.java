package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.TimeStampService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.services.WorkPackageService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import at.qe.sepm.skeleton.ui.beans.TimeStampBean;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Some very basic tests for {@link UserService}.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class TimeStampServiceTest {

	@Autowired
	TimeStampService timeStampService;

	@Autowired
	UserService userInfo;

	@Autowired
	WorkPackageService workPackageInfo;

	@Test
	@WithMockUser(username = "admin", authorities = { "STUDENT" })
	public void testCheckInAndOut() {
		User admin = userInfo.loadUser("admin");
		timeStampService.checkIn(workPackageInfo.getAllPackages().get(0), admin);
		TimeStamp toCheckOut = timeStampService.getOpenTimeStamp4User(admin);
		Assert.assertNotNull("CheckIn did not work", toCheckOut);
		if (toCheckOut != null) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) { 
				//Not necessary...
			}
			timeStampService.checkOut(toCheckOut);
			Assert.assertNull("CheckOut did not work", timeStampService.getOpenTimeStamp4User(admin));
		}
	}

}
