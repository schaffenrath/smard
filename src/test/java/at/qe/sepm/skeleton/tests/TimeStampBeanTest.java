package at.qe.sepm.skeleton.tests;

import at.qe.sepm.skeleton.Main;
import at.qe.sepm.skeleton.model.UserRole;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import at.qe.sepm.skeleton.ui.beans.TimeStampBean;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Some very basic tests for {@link UserService}.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Main.class)
@WebAppConfiguration
public class TimeStampBeanTest {

    @Autowired
    TimeStampBean timeStampBean;
    
    @Test
    @WithMockUser(username = "user1", authorities = {"STUDENT"})
    public void testHasOpenTimeStamp() {
        Assert.assertTrue("timeStampBean.hasOpenTimeStamp does not return true for authenticated user", timeStampBean.hasOpenTimeStamp());
    }

}
