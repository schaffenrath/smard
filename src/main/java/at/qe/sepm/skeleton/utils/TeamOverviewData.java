package at.qe.sepm.skeleton.utils;

import java.util.TreeMap;

import at.qe.sepm.skeleton.model.User;

import java.util.Map;

public class TeamOverviewData {

	private double timeSum = 0.0;
	private int noOfUsers = 0;
	private Map<User, Double> subSums = new TreeMap<>();
	
	public void addTime(User user, double time){
		timeSum += time;
		noOfUsers++;
		subSums.put(user, time);
	}

	public double getTimeSum() {
		return timeSum;
	}
	
	public int getNoOfUsers() {
		return noOfUsers;
	}	
	
	public double getAverage(){
		return timeSum / noOfUsers;
	}
	
	public Map<User,Double> getSubSums(){
		return subSums;
	}
	
}
