package at.qe.sepm.skeleton.email;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.services.UserService;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

@Configuration
public class NotificationManager implements InitializingBean, DisposableBean
{
	@Autowired
	private UserService userService;
	@Autowired
	private ProjectTeamService teamService;
	
    Scheduler sched;
    
    public void afterPropertiesSet() throws SchedulerException 
    {
    	sched = new StdSchedulerFactory().getScheduler();
    	
    	/* Set references so the job can call its methods */
    	sched.getContext().put("userService", userService);
    	sched.getContext().put("teamService", teamService);
 
    	sched.start();
    	
    	/* Create job. */
    	JobDetail job = JobBuilder
    			.newJob(NotificationJob.class)
    			.build();
    	
    	/* Create trigger for job. */
    	CronTrigger trigger = TriggerBuilder
    			.newTrigger()
    			.withSchedule(CronScheduleBuilder.cronSchedule("0 0 0 ? * SUN")) // run job every SUNDAY at 00:00
    			.build();
    	
    	/* Add job and trigger to scheduler. */
    	sched.scheduleJob(job, trigger);
    }
    
    public void destroy() throws Exception
    {
    	sched.shutdown();
    }
}