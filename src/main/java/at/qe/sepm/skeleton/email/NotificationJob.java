package at.qe.sepm.skeleton.email;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.utils.TeamOverviewData;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

@Service
@Configurable
public class NotificationJob implements Job
{ 	
    private UserService userService;
    private ProjectTeamService teamService;
    
    @Override
	public void execute(JobExecutionContext context) throws JobExecutionException
	{
		System.out.println("\n\n\nJOB START\n\n\n");
		
		/* Access references to services. */
		SchedulerContext schedContext = null;
        try 
        {
            schedContext = context.getScheduler().getContext();
        } 
        catch (SchedulerException e) 
        {
            e.printStackTrace();
            return;
        }
        
        /* Get references to services. */
        this.userService = (UserService) schedContext.get("userService");
        this.teamService = (ProjectTeamService) schedContext.get("teamService");
		
        /* Generate statistics and send it to all moderators. */
		SendStatistics();
		
		System.out.println("\n\n\nJOB END\n\n\n");
	}
    
    private void authenticate_login(String role)
    {
    	Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(role);
        Authentication authentication = new UsernamePasswordAuthenticationToken(
                "user",
                role,
                authorities
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
    
    private void authenticate_logout()
    {
    	SecurityContextHolder.getContext().setAuthentication(null);
    }
	
    /* Simple wrapper for SendMessage */
	private void SendMessage(String to, String from, String subject, String message)
	{
		try
		{
			Notifier notifier = new EmailNotifier("smtp.gmail.com", "587", "mailsmard@gmail.com", "mausaus123");
			notifier.SendMessage(to, from, subject, message);
		}
		catch(AddressException e)
		{
			System.out.println("ERROR: AddressException: " + e.getMessage());
		}
		catch(MessagingException e)
		{
			System.out.println("ERROR: MessagingException: " + e.getMessage());
		}
	}
	
	private String getCurrentTime()
	{
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return df.format(Calendar.getInstance().getTime());
	}

	private void SendStatistics()
	{
		authenticate_login("ADMIN");
		List<ProjectTeam> projects = teamService.getAllProjects();
		authenticate_login("MODERATOR");
		
		/* Go through each project and send one mail per project. */
		for(ProjectTeam project : projects)
		{
			Map<WorkPackage, TeamOverviewData> entries = teamService.getOverview4ProjectTeam(project);

			/* HTML consists of header, content and footer. */
			StringBuilder msg_header = new StringBuilder("<!DOCTYPE HTML><html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>");
			msg_header.append("</head><body><div style='padding:10px;'>");
			StringBuilder msg_content = new StringBuilder();
			StringBuilder msg_footer = new StringBuilder("</div></body></html>");
			
			/* Build content. */
			
			msg_content.append("<h2>Workpackages Overview</h2>");
			
			int noUser  = 0;
			int timeSum = 0;
			double timeAvg = 0;
			
			if(entries.size() != 0)
			{
				msg_content.append("<table style='text-align:left;'>");
				msg_content.append("<tr>");
				msg_content.append("<th style='padding:5px; outline:2px solid black;'>WorkPackage</th>");
				msg_content.append("<th style='padding:5px; outline:2px solid black;'>Number of students</th>");
				msg_content.append("<th style='padding:5px; outline:2px solid black;'>Total hours worked</th>");
				msg_content.append("<th style='padding:5px; outline:2px solid black;'>Total average hours worked</th>");
				msg_content.append("</tr>");
			
				for(Map.Entry<WorkPackage, TeamOverviewData> entry : entries.entrySet())
				{
					msg_content.append("<tr>");
					msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + entry.getKey().getWorkPackageName() + "</td>");
					msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + entry.getValue().getNoOfUsers() + "</td>");
					msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + entry.getValue().getTimeSum() + "</td>");
					msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + entry.getValue().getAverage() + "</td>");
					msg_content.append("</tr>");
					
					noUser  += entry.getValue().getNoOfUsers();
					timeSum += entry.getValue().getTimeSum();
				}
				
				timeAvg = (double) timeSum / (double) noUser;
				
				msg_content.append("</table><br>");
			}
			else
			{
				msg_content.append("(empty)");
			}
			
			List<User> users = userService.getUser4ProjectTeam(project);
			
			msg_content.append("<h2>Students Overview</h2>");
			
			msg_content.append("<table style='text-align:left;'>");
			msg_content.append("<tr>");
			msg_content.append("<th style='padding:5px; outline:2px solid black;'>Firstname</th>");
			msg_content.append("<th style='padding:5px; outline:2px solid black;'>Lastname</th>");
			msg_content.append("<th style='padding:5px; outline:2px solid black;'>E-Mail</th>");
			msg_content.append("<th style='padding:5px; outline:2px solid black;'>Phone Number</th>");
			msg_content.append("<th style='padding:5px; outline:2px solid black;'>Number of WorkPackages");
			msg_content.append("<th style='padding:5px; outline:2px solid black;'>WorkPackages involved in</th>");
			msg_content.append("<th style='padding:5px; outline:2px solid black;'>Total number of hours</th>");
			msg_content.append("</tr>");
			
			for(User user : users)
			{
				Map<WorkPackage, Double> overview = userService.getOverview4User(user);
				
				msg_content.append("<tr>");
				msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + user.getFirstName() + "</td>");
				msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + user.getLastName() + "</td>");
				msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + user.getEmail() + "</td>");
				msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + user.getPhone()+ "</td>");
				
				if(overview.size() != 0)
					msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + overview.size() + "</td>");
				else
					msg_content.append("none");
				
				msg_content.append("<td style='padding:0px; outline:2px solid black;'>");
				
				double sum = 0;
				if(overview.entrySet().size() != 0)
				{
					msg_content.append("<table style='text-align:left; margin:0px; padding:0px;'>");
					msg_content.append("<tr>");
					msg_content.append("<th style='padding:5px; outline:2px solid black;'>WorkPackage</th>");
					msg_content.append("<th style='padding:5px; outline:2px solid black;'>Hours worked</th></tr>");
					
					for(Map.Entry<WorkPackage, Double> entry : overview.entrySet())
					{
						msg_content.append("<tr>");
						msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + entry.getKey().getWorkPackageName() + "</td>");
						msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + entry.getValue().toString() + "</td>");
						msg_content.append("</tr>");
						
						sum += entry.getValue();
					}
					msg_content.append("</table>");
				}
				else
				{
					msg_content.append("none");
				}
				
				msg_content.append("</td>");
				msg_content.append("<td style='padding:5px; outline:2px solid black;'>" + sum + "</td>");
				msg_content.append("</tr>");
			}
			
			/* Build summary. */
			
			msg_header.append("<h1>Summary of project " +  project.getProjectTeamName() + " on " + getCurrentTime() + "</h1>");
			msg_header.append("<h2>Project Overview</h2>");
			
			msg_header.append("<table style='text-align:left'>");
			
			msg_header.append("<tr>");
			msg_header.append("<th style='padding:5px; outline:2px solid black;'>Number of workpackages</th>");
			msg_header.append("<td style='padding:5px 5px 5px 10px; outline:2px solid black;'>" + entries.size() + "</td>");
			msg_header.append("</tr>");
			
			msg_header.append("<tr>");
			msg_header.append("<th style='padding:5px; outline:2px solid black;'>Number of students</th>");
			msg_header.append("<td style='padding:5px 5px 5px 10px; outline:2px solid black;'>" + users.size() + "</td>");
			msg_header.append("</tr>");
			
			msg_header.append("<tr>");
			msg_header.append("<th style='padding:5px; outline:2px solid black;'>Total hours worked</th>");
			msg_header.append("<td style='padding:5px 5px 5px 10px; outline:2px solid black;'>" + timeSum + "</td>");
			msg_header.append("</tr>");
			
			msg_header.append("<tr>");
			msg_header.append("<th style='padding:5px; outline:2px solid black;'>Total average hours worked</th>");
			msg_header.append("<td style='padding:5px 5px 5px 10px; outline:2px solid black;'>" + timeAvg + "</td>");
			msg_header.append("</tr>");
			
			msg_header.append("</table>");
			
			msg_content.append("<br><br><div style='width:100%; text-align:center; font-size:10px;margin-top:20px;'><hr>");
			msg_content.append("If you have any questions or problems you can contact the administrator: <a href='mailto:alin.porcic@gmail.com'>alin.porcic@gmail.com</a>");
			msg_content.append("</div>");
			
			String mail_to = project.getModerator().getEmail();
			
			if(mail_to == null || mail_to == "")
			{
				System.out.println("ERROR: no email defined for user " + project.getModerator().getUsername() + "!");
				return;
			}
			
			SendMessage(project.getModerator().getEmail(), "mailsmard@gmail.com",
					"Summary of project " + project.getProjectTeamName(), msg_header.append(msg_content).append(msg_footer).toString());
			
			authenticate_logout();
		}
	}	
}