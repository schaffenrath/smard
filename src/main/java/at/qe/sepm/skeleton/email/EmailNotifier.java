package at.qe.sepm.skeleton.email;

import at.qe.sepm.skeleton.email.Notifier;
import java.lang.RuntimeException;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailNotifier implements Notifier
{
	private Session session;

	public EmailNotifier(String host, String port, final String username, final String password)
	{
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);

		session = Session.getInstance(props, new javax.mail.Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {
		        return new PasswordAuthentication(username, password);
		    }
		});
	}

	public void SendMessage(String to, String from, String subject, String message) throws AddressException, MessagingException 
	{
		MimeMessage m = new MimeMessage(session);
		m.setContent(message, "text/html; charset=utf-8");
		m.setFrom(new InternetAddress(from));
		m.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
		m.setSubject(subject);
		Transport.send(m);
	}
}