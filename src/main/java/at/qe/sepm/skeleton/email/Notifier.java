package at.qe.sepm.skeleton.email;

 

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public interface Notifier
{
	public void SendMessage(String to, String from, String subject, String message) throws AddressException, MessagingException; 
}