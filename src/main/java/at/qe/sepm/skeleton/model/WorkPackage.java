package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing users.
 */
@Entity
public class WorkPackage implements Persistable<String>, Comparable<WorkPackage>{

    private static final long serialVersionUID = 1L;

    @Id
    private Long workPackageId;
    
    private String workPackageName;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    
        
    
    public Long getWorkPackageId() {
		return workPackageId;
	}

	public void setWorkPackageId(Long workPackageId) {
		this.workPackageId = workPackageId;
	}

	public String getWorkPackageName() {
		return workPackageName;
	}

	public void setWorkPackageName(String workPackageName) {
		this.workPackageName = workPackageName;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.workPackageId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof WorkPackage)) {
            return false;
        }
        final WorkPackage other = (WorkPackage) obj;
        if (!Objects.equals(this.workPackageId, other.workPackageId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "at.qe.sepm.skeleton.model.WorkPackage[ id=" + workPackageId + " ]";
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public boolean isNew() {
		return (null == createDate);
	}

	//Needed for Key in TreeMap
	@Override
	public int compareTo(WorkPackage o) {
		return this.getWorkPackageName().compareTo(o.getWorkPackageName());
	}


}
