package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing ProjectTeam.
 */
@Entity
public class ProjectTeam implements Persistable<String> {

    private static final long serialVersionUID = 1L;

    @Id
    private Long projectTeamId;
    
    private String projectTeamName;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @ManyToOne @JoinColumn(nullable = true, name="moderator")
    private User moderator;	//Moderator
    
    public Long getProjectTeamId() {
		return projectTeamId;
	}
    
	public void setProjectTeamId(Long projectTeamId) {
		this.projectTeamId = projectTeamId;
	}

	public String getProjectTeamName() {
		return this.projectTeamName;
	}

	public void setProjectTeamName(String projectTeamName) {
		this.projectTeamName = projectTeamName;
	}

	public User getModerator() {
		return moderator;
	}

	public void setModerator(User moderator) {
		this.moderator = moderator;
	}
	
    public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.projectTeamId);
        return hash;
    }



	@Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof ProjectTeam)) {
            return false;
        }
        final ProjectTeam other = (ProjectTeam) obj;
        if (!Objects.equals(this.projectTeamId, other.projectTeamId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "at.qe.sepm.skeleton.model.ProjectTeam[ id=" + projectTeamId + " ]";
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public boolean isNew() {
		return (null == createDate);
	}


}
