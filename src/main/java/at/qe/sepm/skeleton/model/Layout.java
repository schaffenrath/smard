package at.qe.sepm.skeleton.model;

public enum Layout {
	DEFAULT,
    DaBaDeeDaBaDie,
    CottonCandy,
	Matrix
}
