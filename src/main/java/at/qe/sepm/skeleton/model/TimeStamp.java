package at.qe.sepm.skeleton.model;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.domain.Persistable;

/**
 * Entity representing timestamps.
 */
@Entity
public class TimeStamp implements Persistable<String> {

    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy=GenerationType.AUTO)
    private Long timeStampId;

    private Date timeStampStart;
    
    private Date timeStampEnd;

	private Double time;
    
    @ManyToOne @JoinColumn(nullable = true, name="user")
    private User user;
    
    @ManyToOne @JoinColumn(nullable = true, name="workPackage")
    private WorkPackage workPackage;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;    
    
    

    public Long getTimeStampId() {
		return timeStampId;
	}

	public void setTimeStampId(Long timeStampId) {
		this.timeStampId = timeStampId;
	}

	public Date getTimeStampStart() {
		return timeStampStart;
	}

	public void setTimeStampStart(Date timeStampStart) {
		this.timeStampStart = timeStampStart;
	}

	public Date getTimeStampEnd() {
		return timeStampEnd;
	}

	public void setTimeStampEnd(Date timeStampEnd) {
		this.timeStampEnd = timeStampEnd;
	}

	public Double getTime() {
		return time;
	}

	public void calcTime() {
		this.time = ((this.getTimeStampEnd().getTime() - this.getTimeStampStart().getTime()) / (double) (60*60*1000));
	}

	public User getUser() {
		return this.user;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
    
    public WorkPackage getWorkPackage() {
		return workPackage;
	}

	public void setWorkPackage(WorkPackage workPackage) {
		this.workPackage = workPackage;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.timeStampId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof TimeStamp)) {
            return false;
        }
        final TimeStamp other = (TimeStamp) obj;
        if (!Objects.equals(this.timeStampId, other.timeStampId)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "at.qe.sepm.skeleton.model.TimeStamp[ id=" + timeStampId + " ]";
    }

    @Override
    public String getId() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

	@Override
	public boolean isNew() {
		return (null == createDate);
	}


}
