package at.qe.sepm.skeleton.repositories;

import java.util.List;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;

/**
 * Repository for managing {@link User} entities.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
public interface WorkPackageRepository extends AbstractRepository<WorkPackage, Long> {

    WorkPackage findFirstByWorkPackageId(Long workPackageId);
    
    WorkPackage findFirstByWorkPackageName(String workPackageName);

    List<WorkPackage> findByWorkPackageNameContaining(String workPackageName);
}
