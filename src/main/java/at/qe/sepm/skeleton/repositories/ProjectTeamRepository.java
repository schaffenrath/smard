package at.qe.sepm.skeleton.repositories;

import java.util.List;

import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.model.User;

/**
 * Repository for managing {@link User} entities.
 *
 * @author
 */
public interface ProjectTeamRepository extends AbstractRepository<ProjectTeam, Long> {

    ProjectTeam findFirstByProjectTeamId(Long projectTeamId);
    
    ProjectTeam findFirstByProjectTeamName(String projectTeamName);

    List<ProjectTeam> findByProjectTeamNameContaining(String projectTeamName);
    
    List<ProjectTeam> findAllByModerator(User moderator);
}
