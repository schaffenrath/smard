package at.qe.sepm.skeleton.repositories;

import java.util.List;

import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;

/**
 * Repository for managing {@link User} entities.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
public interface TimeStampRepository extends AbstractRepository<TimeStamp, Long> {

    TimeStamp findFirstByTimeStampId(Long timeStampId);
    
    List<TimeStamp> findByUser(User username);
    
    List<TimeStamp> findByWorkPackage(WorkPackage workPackage);
    
    List<TimeStamp> findAllByWorkPackageAndTimeStampEndIsNotNull(WorkPackage workPackage);
    
    //@Query("SELECT t FROM Timestamp t WHERE t.user = :user AND timeStampEnd IS NULL LIMIT 1")
    //TimeStamp findOpenTimeStamp(@Param("user") User user);
    
    TimeStamp findFirstByUserAndTimeStampEndIsNull(User username);
    
    List<TimeStamp> findAllByUser(User username);

    List<TimeStamp> findAllByUserAndTimeStampEndIsNotNull(User username);
    
}
