package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.WorkPackageService;

import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the workPackages list view.
 *
 * @author Robert Schaffenrath
 */
@Component
@Scope("view")
public class WorkPackageListController {

    @Autowired
    private WorkPackageService workPackageService;

    /**
     * Returns a list of all packages.
     *
     * @return
     */
    public Collection<WorkPackage> getPackages() {
        return workPackageService.getAllPackages();
    }

}
