package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.TimeStampService;
import at.qe.sepm.skeleton.services.WorkPackageService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the workPackage detail view.
 *
 * @author Robert Schaffenrath
 */
@Component
@Scope("view")
public class WorkPackageDetailController {

    @Autowired
    private WorkPackageService workService;
    
    @Autowired
    private TimeStampService timeStampService;
    
    @Autowired
    private SessionInfoBean sessionInfo;

    /**
     * Attribute to cache the currently selected workPackage
     */
    private WorkPackage workPackage;
    
    /**
     * Attribute to cache the currently selected workPackageId
     */
    private Long selectedId; 

    /**
     * Sets the workPackage 
     * @param workPackage 
     */
    public void setWorkPackage(WorkPackage workPackage) {
        this.workPackage = workPackage;
        doReloadWorkPackage();
    }

    /**
     * Returns the currently selected workPackage
     * @return 
     */
    public WorkPackage getWorkPackage() {
        return workPackage;
    }
    
    /**
     * Sets the currently selected workPackage by id
     * selectOneMenu saves workPackageId
     * @param id 
     */
    public void setWorkPackageId(Long id){
        this.selectedId=id;
    }
    
    /**
     * Returns the currently selected username
     * Used to load workPackage for selectOneMenu
     * @return 
     */
    public Long getWorkPackageId(){
        return this.selectedId;
    }
    
    /**
     * Action loads workPackage by id
     * used for selectOneMenu to select workPackage
     */
    public void loadWorkPackageWithId(){
        this.setWorkPackage(workService.loadPackageWithId(selectedId));
    }
    
    
    /**
     * Sets the currently selected workPackage and reloads it form db. This workPackage is
     * targeted by any further calls of
     * {@link #doReloadWorkPackage()}, {@link #checkIn()} and
     * {@link #checkOut()}.
     *
     * @param user
     */
    
    /**
     * Action to force a reload of the currently displayed workPackage.
     */
    public void doReloadWorkPackage() {
        workPackage = workService.loadPackageWithName(workPackage.getWorkPackageName());
    }
    
    
    /**
     * Action to create starting timeStamp with selected workPackage
     */
    public void checkIn(){
        if(this.workPackage != null)
            timeStampService.checkIn(this.workPackage, sessionInfo.getCurrentUser());
    }
    
    /**
     * Action to creat closing timeStamp for starting timeStamp
     */
    public void checkOut(){
        timeStampService.checkOut(timeStampService.getOpenTimeStamp4User(sessionInfo.getCurrentUser()));
    }
    
    /**
     * Returns the name of the work package for the currently open timeStamp for the 
     * logged in user 
     * @return 
     */
    public String getOpenWorkPackageName() {
        return timeStampService.getOpenTimeStamp4User(sessionInfo.getCurrentUser()).getWorkPackage().getWorkPackageName();
    }
    
    /**
     * Sets the currently saved workPakage to null
     */
    public void doDeleteWorkPackage() {
        this.workService.deleteWorkPackage(workPackage);
        workPackage = null;
    }
    
    /**
     * Saves the currently saved workPackage via the service
     */
    public void doSaveWorkPackage() {
        workPackage = this.workService.saveWorkPackage(workPackage);
    }
}
