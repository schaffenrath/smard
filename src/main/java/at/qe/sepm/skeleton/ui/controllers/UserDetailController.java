package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the user detail view.
 *
 * @author Robert Schaffenrath
 */
@Component
@Scope("view")
public class UserDetailController {

	@Autowired
	private UserService userService;

	@Autowired
	private SessionInfoBean sessionBean;

	/**
	 * Attribute to cache the currently selected user
	 */
	private User user;

	/**
	 * Attribute to cache the currently selected username used for selectOneMenu
	 */
	private String selectName;

	/**
	 * Returns the currently selected user.
	 *
	 * @return
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Returns the currently selected username Used to load user for
	 * selectOneMenu
	 * 
	 * @return
	 */
	public String getUserName() {
		return selectName;
	}

	/**
	 * Sets the username selectOneMenu saves username
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.selectName = userName;
	}

	/**
	 * Action to load user by username used for selectOneMenu to select user
	 */
	public void doLoadUserByName() {
		if (this.selectName != null) {
                    this.user = userService.loadUser(selectName);
		}
	}

	/**
	 * Sets the currently displayed user and reloads it form db. This user is
	 * targeted by any further calls of {@link #doReloadUser()},
	 * {@link #doSaveUser()} and {@link #doDeleteUser()}.
	 *
	 * @param user
	 */
	public void setUser(User user) {
		this.user = user;
		doReloadUser();
	}

	/**
	 * Action to force a reload of the currently displayed user.
	 */
	public void doReloadUser() {
		user = userService.loadUser(user.getUsername());
	}

	/**
	 * Action to save the currently selected user.
	 */
	public void doSaveUser() {
		user = this.userService.saveUser(user);
	}

	/**
	 * Action to delete the currently selected user.
	 */
	public void doDeleteUser() {
		this.userService.deleteUser(user);
		user = null;
	}

	/**
	 * Action loads workPackage and Time for saved user and converts it to
	 * Set of Entries for dataTable
	 * 
	 * @return
	 */
	public Set<Map.Entry<String, Double>> getOverview4User() {
		if (this.user != null) {
			Map<WorkPackage, Double> userProjectSet = userService.getOverview4User(this.user);
			Set<WorkPackage> keys = userProjectSet.keySet();
			Map<String, Double> outputMap = new TreeMap<>();
			for (WorkPackage key : keys) {
				Long hours = Math.round(userProjectSet.get(key) * 100);
				Double rounded = hours.doubleValue() / 100;

				outputMap.put(key.getWorkPackageName(), rounded);
			}
			return outputMap.entrySet();
		} else {
			return null;
		}
	}

	/**
	 * Checks if overview4Users is loaded or available
	 * 
	 * @return true if overview4Users is available and loaded, false otherwise
	 */
	public boolean isUserSelected() {
		return this.user != null;
	}

	public List<String> getDesigns() {
		return userService.getAllDesigns();
	}

	public String getDesign() {
		return userService.getUserDesign(sessionBean.getCurrentUser());
	}

	public void setDesign(String design) {
		userService.setUserDesign(sessionBean.getCurrentUser(), design);
	}
}