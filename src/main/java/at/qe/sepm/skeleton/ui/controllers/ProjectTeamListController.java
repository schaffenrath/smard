package at.qe.sepm.skeleton.ui.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;

/**
 * Controller for the project list view.
 *
 * @author Robert Schaffenrath
 */
@Component
@Scope("view")
public class ProjectTeamListController {

    @Autowired
    private ProjectTeamService projectTeamService;
    @Autowired
    private SessionInfoBean sessionInfo;

    /**
     * Returns a list of all projects.
     *
     * @return
     */
    public Collection<ProjectTeam> getProjects() {
        return projectTeamService.loadAllProjects4Moderator(sessionInfo.getCurrentUser());
    }
    
    public Collection<ProjectTeam> getAdminProjects() {
        return projectTeamService.getAllProjects();
    }

}
