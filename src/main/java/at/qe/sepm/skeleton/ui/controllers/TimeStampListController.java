package at.qe.sepm.skeleton.ui.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.services.TimeStampService;

/**
 * Controller for the timeStamp list view.
 *
 * @author Robert Schaffenrath
 */
@Component
@Scope("view")
public class TimeStampListController {

    @Autowired
    private TimeStampService timeStampService;

    /**
     * Returns a list of all stamps.
     *
     * @return
     */
    public Collection<TimeStamp> getStamps() {
        return timeStampService.getAllStamps();
    }

}
