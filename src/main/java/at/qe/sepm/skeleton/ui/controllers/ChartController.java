/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.services.TimeStampService;
import at.qe.sepm.skeleton.utils.TeamOverviewData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.function.Consumer;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;
import org.primefaces.model.chart.PieChartModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author robert
 */
@Component
@Scope("view")
public class ChartController {

	@Autowired
	private TimeStampService stampService;

	@Autowired
	private ProjectTeamService projectTeamService;

	@Autowired
	private ProjectTeamDetailController teamController;

	/**
	 * User statistic Graph
	 */
	private HorizontalBarChartModel userChart;
	private HorizontalBarChartModel groupChart;
	private PieChartModel groupPieChart;
	private List<PieChartModel> groupWpPieCharts;

	private int height;
	private int width;
	private boolean userChartLoaded;
	private boolean groupChartLoaded;

	public HorizontalBarChartModel getUserChart() {
		return userChart;
	}

	public HorizontalBarChartModel getGroupChart() {
		return groupChart;
	}

	public PieChartModel getGroupPieChart() {
		return groupPieChart;
	}

	public List<PieChartModel> getGroupWpPieCharts() {
		return groupWpPieCharts;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public boolean isUserChartLoaded() {
		return this.userChartLoaded;
	}

	public boolean isGroupChartLoaded() {
		return this.groupChartLoaded;
	}

	public void createUserChart(User user) {
		this.height = 60;
		this.width = 0;
		this.userChartLoaded = false;
		if (user != null) {
			this.userChart = new HorizontalBarChartModel();
			List<TimeStamp> stamps = stampService.getClosedTimeStamps4User(user);
			ChartSeries chartInfo = new ChartSeries();

			for (TimeStamp stamp : stamps) {
				this.userChartLoaded = true;
				this.height += 60;
				if (stamp.getTime() > this.width)
					this.width = stamp.getTime().intValue();
				chartInfo.set(stamp.getWorkPackage().getWorkPackageName() + ": " + this.dateConverter(stamp),
						stamp.getTime());
			}
			userChart.addSeries(chartInfo);

			userChart.setTitle("Time per check in");
			userChart.setSeriesColors("5BC0EB,FA7921,60495A,9BC53D,E55934,FDE74C,2B2D42");
			userChart.setExtender("chartExtender");
			userChart.setAnimate(true);
			userChart.setMouseoverHighlight(true);
			userChart.setBarWidth(30);
			userChart.setShadow(true);

			Axis xAxis = userChart.getAxis(AxisType.X);
			xAxis.setLabel("Hours");
			xAxis.setMin(0);

			if (Math.round(this.width) < this.width) {
				this.width = Math.round(this.width + 1);
			} else {
				this.width = Math.round(this.width);
			}

			xAxis.setMax(this.width);
			xAxis.setTickCount(11);

			Axis yAxis = userChart.getAxis(AxisType.Y);
			yAxis.setLabel("Work Package & Start time");
		}

	}

	public void onGroupComplete(ProjectTeam team) {
		if (team != null) {
			createGroupChart(team);
			createGroupPieChart(team);
			Set<Entry<WorkPackage, TeamOverviewData>> overview = teamController.getOverview4PiChart(team);
			groupWpPieCharts = new ArrayList<>();
			overview.forEach(new Consumer<Map.Entry<WorkPackage, TeamOverviewData>>() {
				@Override
				public void accept(Entry<WorkPackage, TeamOverviewData> t) {
					groupWpPieCharts.add(createGroupWpPieChart(t));
				}
			});
		} else {
			groupChartLoaded = false;
		}
	}

	public void createGroupChart(ProjectTeam team) {
		this.groupChartLoaded = false;
		this.height = 60;
		this.width = 0;
		if (team != null) {
			this.groupChart = new HorizontalBarChartModel();

			Set<Map.Entry<String, Double>> teamData = teamController.getOverview4ProjectTeam();

			ChartSeries chartInfo = new ChartSeries();

			for (Map.Entry<String, Double> t : teamData) {
				this.groupChartLoaded = true;
				this.height += 60;
				if (t.getValue() > this.width) {
					this.width = t.getValue().intValue() + 1;
				}
				chartInfo.set(t.getKey(), t.getValue());
			}

			groupChart.addSeries(chartInfo);

			groupChart.setTitle("Time per work package");
			groupChart.setSeriesColors("5BC0EB,FA7921,60495A,9BC53D,E55934,FDE74C,2B2D42");
			groupChart.setExtender("chartExtender");
			groupChart.setAnimate(true);
			groupChart.setMouseoverHighlight(true);
			groupChart.setBarWidth(30);
			groupChart.setShadow(true);

			Axis xAxis = groupChart.getAxis(AxisType.X);
			xAxis.setLabel("Hours");
			xAxis.setMin(0);

			if (Math.round(this.width) < this.width) {
				this.width = Math.round(this.width + 1);
			} else {
				this.width = Math.round(this.width);
			}

			xAxis.setMax(width);
			xAxis.setTickCount(11);

			Axis yAxis = groupChart.getAxis(AxisType.Y);
			yAxis.setLabel("Work packages");

		}

	}

	private void createGroupPieChart(ProjectTeam team) {
		groupPieChart = new PieChartModel();
		// For Each entry in the TotalTimes Map add to PieChart
		projectTeamService.getTotalTimesProject(team).entrySet().forEach(new Consumer<Map.Entry<User, Double>>() {
			@Override
			public void accept(Entry<User, Double> t) {
				groupPieChart.set(t.getKey().getUsername(), t.getValue());
			}
		});

		groupPieChart.setTitle("Time for ProjectTeam [" + team.getProjectTeamName() + "]");
		groupPieChart.setShowDataLabels(true);
		groupPieChart.setLegendPosition("e");

	}

	private PieChartModel createGroupWpPieChart(Map.Entry<WorkPackage, TeamOverviewData> overviewEntry) {
		PieChartModel newPieChart = new PieChartModel();
		Map<User, Double> userStatistic = overviewEntry.getValue().getSubSums();
		for (Map.Entry<User, Double> entry : userStatistic.entrySet()) {
			newPieChart.set(entry.getKey().getUsername(), entry.getValue());
		}

		newPieChart.setTitle("Time for [" + overviewEntry.getKey().getWorkPackageName() + "]");
		newPieChart.setShowDataLabels(true);
		newPieChart.setLegendPosition("e");
		return newPieChart;

	}

	public String dateConverter(TimeStamp stamp) {
		String date = stamp.getTimeStampStart().toString().substring(11, 13) + ":" + // hour
				stamp.getTimeStampStart().toString().substring(14, 16) + " " + // minute
				stamp.getTimeStampStart().toString().substring(8, 10) + "." + // day
				stamp.getTimeStampStart().toString().substring(5, 7) + "." + // month
				stamp.getTimeStampStart().toString().substring(0, 4); // year
		return date;
	}

}
