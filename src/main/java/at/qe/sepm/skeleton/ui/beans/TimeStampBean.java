package at.qe.sepm.skeleton.ui.beans;

import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.services.TimeStampService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author daniel
 */

@Component
@Scope("request")
public class TimeStampBean {
    
    @Autowired
    private TimeStampService timeStampService;
    
    @Autowired
    private SessionInfoBean sessionInfo;
    
    public boolean hasOpenTimeStamp(){
            return hasOpenTimeStamp4User(sessionInfo.getCurrentUser());
    }
    
    public boolean hasOpenTimeStamp4User(User user){
        return timeStampService.getOpenTimeStamp4User(user) != null;
    }
    
    public TimeStamp getOpenTimeStamp(){
        return timeStampService.getOpenTimeStamp4User(sessionInfo.getCurrentUser());
    }
    
    public List<TimeStamp> getStamps4User(User user){
        return this.timeStampService.findByUser(user);
    }
}
