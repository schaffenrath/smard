package at.qe.sepm.skeleton.ui.controllers;

import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.services.ProjectTeamService;
import at.qe.sepm.skeleton.services.UserService;
import at.qe.sepm.skeleton.ui.beans.SessionInfoBean;
import at.qe.sepm.skeleton.utils.TeamOverviewData;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Controller for the project detail view.
 *
 * @author Robert Schaffenrath
 */
@Component
@Scope("view")
public class ProjectTeamDetailController {

    @Autowired
    private ProjectTeamService projectService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private SessionInfoBean sessionBean;

    /**
     * Attribute to cache the currently selected projectTeam
     */
    private ProjectTeam projectTeam;
    
    /**
     * Attribute to cache the currently selected projectTeamName
     * used for selectOneMenu
     */
    private String selectName;

    /**
     * Returns the currently selected project.
     *
     * @return
     */
    public ProjectTeam getProjectTeam() {
        return projectTeam;
    }

    /**
     * Returns the currently selected projectTeamName
     * @return 
     */
    public String getSelectName() {
        return selectName;
    }

    /**
     * Sets the projectTeamName
     * selectOneMenu saves projectTeamName
     * @param selectName 
     */
    public void setSelectName(String selectName) {
        this.selectName = selectName;
    }

    /**
     * Sets the currently displayed projectTeam and reloads it form db. This projectTeam is
     * targeted by any further calls of
     * {@link #doReloadProjectTeam()}, {@link #doSaveProjectTeam()} and
     * {@link #doDeleteProjectTeam()}.
     *
     * @param projectTeam
     */
    
    public void setProjectTeam(ProjectTeam projectTeam) {
        this.projectTeam = projectTeam;
        doReloadProjectTeam();
    }

    
    public void doLoadProjectTeamByName(){
        this.projectTeam = projectService.loadProject(selectName);
    }

    public void doDeleteProjectTeam() {
        this.projectService.deleteProjectTeam(projectTeam);
        projectTeam = null;
    }
    
    public void doSaveProjectTeam() {
        projectTeam = this.projectService.saveProjectTeam(projectTeam);
    }

    /**
     * Action to force a reload of the currently displayed projectTeam.
     */
    public void doReloadProjectTeam() {
        projectTeam = projectService.loadProject(projectTeam.getProjectTeamName());
    }
    
    /**
     * Converts overview data from service to Set of work package name and 
     * total time
     * @return 
     */
    public Set<Map.Entry<String, Double>> getOverview4ProjectTeam() {
        if (this.projectTeam != null) {
            Map<WorkPackage, TeamOverviewData> userProjectSet = projectService.getOverview4ProjectTeam(this.projectTeam);
            Set<WorkPackage> keys = userProjectSet.keySet();
            Map<String, Double> outputMap = new TreeMap<>();
            for (WorkPackage key : keys) {
                    outputMap.put(key.getWorkPackageName(), userProjectSet.get(key).getTimeSum());
            }
            return outputMap.entrySet();
            //return new ArrayList<>(outputMap.entrySet());
        } else {
            return null;
        }
    }
    
    /**
     * Return Set of workPackage and teamOveriewData for specific project team
     * Method is necesary for the pieChart in the ChartController
     * @param team
     * @return 
     */
    public Set<Map.Entry<WorkPackage, TeamOverviewData>> getOverview4PiChart(ProjectTeam team){
        if (team != null){
            return projectService.getOverview4ProjectTeam(team).entrySet();
        }
        else{
            return null;
        }
    }
    
    /**
     * Checks if there is currently a projectTeam saved in this controller
     * @return true if projectTeams is saved, false otherwise 
     */
    public boolean isProjectSelected() {
        return this.projectTeam != null;
    }
    
    /**
     * Returns a list of all project teams
     * @return 
     */
    public List<ProjectTeam> getProjectTeams() {
        return projectService.loadAllProjects4Moderator(sessionBean.getCurrentUser());
    }
    
    /**
     * Returns set of all users, who are working on the currently saved project
     * @return 
     */
    public Set<User> getUserForProjectTeam() {      
        List<ProjectTeam> teams = getProjectTeams();
        
        //updated by GOGLDA - Instead of List used SortedSet
        Set<User> teamUsers = new TreeSet<>();
        for (ProjectTeam t : teams) {
            teamUsers.addAll(userService.getUser4ProjectTeam(t));
        }
        teamUsers.add(sessionBean.getCurrentUser());
        
        return teamUsers;
                
    }
    
}
