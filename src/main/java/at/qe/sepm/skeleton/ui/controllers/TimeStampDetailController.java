package at.qe.sepm.skeleton.ui.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.services.TimeStampService;

/**
 * Controller for the timeStamp detail view.
 *
 * @author Robert Schaffenrath
 */
@Component
@Scope("view")
public class TimeStampDetailController {

    @Autowired
    private TimeStampService timeService;
   
    /**
     * Attribute to cache the currently selected timeStamp
     */
    private TimeStamp timeStamp;

    /**
     * Sets the currently selected timeStamp and reloads it form db. This timeStamp is
     * targeted by any further calls of
     * {@link #doReloadTimeStamp()}.
     *
     * @param timeStamp
     */
    public void setTimeStamp(TimeStamp timeStamp) {
        this.timeStamp = timeStamp;
        doReloadTimeStamp();
    }

    /**
     * Returns the currently selected timeStamp.
     *
     * @return
     */
    public TimeStamp getTimeStamp() {
        return timeStamp;
    }
   
    /**
     * Action to force a reload of the currently selected timeStamp.
     */
    public void doReloadTimeStamp() {
        timeStamp = timeService.loadStamp(timeStamp.getTimeStampId());
    }
    
	/**
	 * Action to delete the currently selected TimeStamp.
	 */
	public void doDeleteTimeStamp() {
		this.timeService.deleteTimeStamp(timeStamp);
		timeStamp = null;
	}
}
