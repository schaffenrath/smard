package at.qe.sepm.skeleton.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.Layout;
import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.repositories.UserRepository;

/**
 * Service for accessing and manipulating user data.
 *
 * @author
 */
@Component
@Scope("application")
public class UserService {

	private final String DEFAULT_DESIGN = "DEFAULT";

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private TimeStampService timeStampService;

	private ProjectTeamService projectTeamService;

	private double subTotal;
	
	/**
	 * Returns a collection of all users.
	 *
	 * @return
	 */
	@PreAuthorize("hasAuthority('ADMIN')")
	public Collection<User> getAllUsers() {
		return userRepository.findAll();
	}

	/**
	 * Loads a single user identified by its username.
	 *
	 * @param username
	 *            the username to search for
	 * @return the user with the given username
	 */
	@PreAuthorize("hasAuthority('ADMIN') or hasAuthority('MODERATOR') or principal.username eq #username")
	public User loadUser(String username) {
		return userRepository.findFirstByUsername(username);
	}

	/**
	 * Saves the user. This method will also set {@link User#createDate} for new
	 * entities or {@link User#updateDate} for updated entities. The user
	 * requesting this operation will also be stored as {@link User#createDate}
	 * or {@link User#updateUser} respectively.
	 *
	 * @param user
	 *            the user to save
	 * @return the updated user
	 */
	@PreAuthorize("hasAuthority('ADMIN')")
	public User saveUser(User user) {
		if (user.isNew()) {
			user.setCreateDate(new Date());
			user.setCreateUser(getAuthenticatedUser());
		} else {
			user.setUpdateDate(new Date());
			user.setUpdateUser(getAuthenticatedUser());
		}
		return userRepository.save(user);
	}

	@PreAuthorize("hasAuthority('MODERATOR')")
	public List<User> getUser4ProjectTeam(ProjectTeam team) {
		return userRepository.findByProjectTeam(team);
	}

	@PreAuthorize("hasAuthority('MODERATOR') or principal.username eq #user.getUsername()")
	public Map<WorkPackage, Double> getOverview4User(User user) {
		List<TimeStamp> tss = timeStampService.getClosedTimeStamps4User(user);
		Map<WorkPackage, Double> sumMap = new TreeMap<>();
		for (TimeStamp ts : tss) {
			WorkPackage wp = ts.getWorkPackage();
			if (sumMap.containsKey(wp)) {
				sumMap.replace(wp, sumMap.get(wp) + ts.getTime());
			} else {
				sumMap.put(wp, ts.getTime());
			}
		}
		return sumMap;
	}

	/**
	 * Deletes the user.
	 *
	 * @param user
	 *            the user to delete
	 */
	@PreAuthorize("hasAuthority('ADMIN')")
	public void deleteUser(User user) {
		projectTeamService.setModeratorToAdmin(user);
		timeStampService.deleteByUser(user);
		userRepository.delete(user);
		// :TODO: write some audit log stating who and when this user was
		// permanently deleted.
	}

	private User getAuthenticatedUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return userRepository.findFirstByUsername(auth.getName());
	}

	public void setProjectTeamService(ProjectTeamService projectTeamService) {
		this.projectTeamService = projectTeamService;
	}

	public void setProjectTeamToNull(User user) {
		user.setProjectTeam(null);
		userRepository.save(user);
	}

	public String getUserDesign(User user) {
		if (user != null) {
			Layout design = user.getDesign();
			if (design != null) {
				return design.toString();
			}
		}
		return DEFAULT_DESIGN;
	}

	@PreAuthorize("hasAuthority('ADMIN') or principal.username eq #user.getUsername()")
	public void setUserDesign(User user, String design) {
		user.setDesign(Layout.valueOf(design));
		saveUser(user);
	}

	public List<String> getAllDesigns() {
		List<String> designs = new ArrayList<>(5);
		for (Layout design : Layout.values()) {
			designs.add(design.toString());
		}
		return designs;
	}

	public double getTotalTime(User user) {
		subTotal = 0.0;
		Map<WorkPackage, Double> overview = getOverview4User(user);
		overview.entrySet().forEach(new Consumer<Entry<WorkPackage, Double>>() {
			@Override
			public void accept(Entry<WorkPackage, Double> t) {
				subTotal += t.getValue();
			}
		});
		return subTotal;
	}

}
