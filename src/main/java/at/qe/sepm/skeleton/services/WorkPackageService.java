package at.qe.sepm.skeleton.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.repositories.TimeStampRepository;
import at.qe.sepm.skeleton.repositories.WorkPackageRepository;
import at.qe.sepm.skeleton.utils.TeamOverviewData;

/**
 * Service for accessing WorkPackage data.
 *
 * @author Daniel Gogl <Daniel.Gogl@student.uibk.ac.at>
 */
@Component
@Scope("application")
public class WorkPackageService {
	
	@Autowired
	private TimeStampService timeStampService;
	@Autowired
	private WorkPackageRepository workPackageRepository;
	@Autowired
	private TimeStampRepository timeStampRepository;

	/**
	 * Returns a collection of all Projects.
	 *
	 * @return
	 */
	@PreAuthorize("hasAuthority('STUDENT')")
	public List<WorkPackage> getAllPackages() {
		return workPackageRepository.findAll();
	}

    /**
     * Loads a single user identified by its username.
     *
     * @param workPackageName
     * @return the user with the given username
     */
    @PreAuthorize("hasAuthority('STUDENT')")
    public WorkPackage loadPackageWithName(String workPackageName) {
        return workPackageRepository.findFirstByWorkPackageName(workPackageName);
    }
    
    @PreAuthorize("hasAuthority('STUDENT')")
    public WorkPackage loadPackageWithId(Long workPackageId){
        return workPackageRepository.findFirstByWorkPackageId(workPackageId);
    }

    /**
     * Loads distinct workpackages where a given user has timestamps
     *
     * @param user
     * @return Distinct list of workpackages
     */
    @PreAuthorize("hasAuthority('MODERATOR') or principal.username eq #user.getUsername()")
	public List<WorkPackage> getPackagesWorkedOn(User user) {
		List<TimeStamp> timeStamps = timeStampRepository.findAllByUser(user);
		List<WorkPackage> wps = new ArrayList<>();
		for (TimeStamp timeStamp : timeStamps) {
			if (!wps.contains(timeStamp.getWorkPackage())) {
				wps.add(timeStamp.getWorkPackage());
			}
		}
		return wps;
	}
	
	
    /**
     * This method merges 2 given overview maps. The result is not a new map. It merges map2 into map1.
     *
     * @param Map1 Overview for Team
     * @param Map2 UserOverview
     * @return Map1 where Map2 is inserted with updated values and maybe new keys
     */
	public Map<WorkPackage, TeamOverviewData> mergeOverview(Map<WorkPackage, TeamOverviewData> mp1, Map<WorkPackage, Double> mp2, User user) {
		if (mp2 != null) {
			for (Map.Entry<WorkPackage, Double> e : mp2.entrySet()) {
				TeamOverviewData data = mp1.get(e.getKey());
				if (data == null) {
					data = new TeamOverviewData();
					mp1.put(e.getKey(), data);
				}
				data.addTime(user, e.getValue());
			}
		}
		return mp1;
	}

	@PreAuthorize("hasAuthority('MODERATOR')")
	public double totalTimeForWorkPackage(WorkPackage workPackage) {
		List<TimeStamp> timeStamps = timeStampService.getClosedTimeStamps4WorkPackage(workPackage);
		double result = 0.0;
		for (TimeStamp ts : timeStamps) {
			result += ts.getTime();
		}
		return result;
	}
	
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteWorkPackage(WorkPackage workPackage) {
    	timeStampService.setTimeStampToNull(workPackage);
    	workPackageRepository.delete(workPackage);
    }
	
    @PreAuthorize("hasAuthority('ADMIN')")
    public WorkPackage saveWorkPackage(WorkPackage workPackage) {
        if (workPackage.isNew()) {
        	workPackage.setCreateDate(new Date());
        }
        return workPackageRepository.save(workPackage);
    }

	@PreAuthorize("hasAuthority('MODERATOR')")
	public Map<User, Double> getOverview4User(WorkPackage workPackage) {
		List<TimeStamp> tss = timeStampService.getClosedTimeStamps4WorkPackage(workPackage);
		Map<User, Double> sumMap = new TreeMap<>();
		for (TimeStamp ts : tss) {
			User user = ts.getUser();
			if (sumMap.containsKey(user)) {
				sumMap.replace(user, sumMap.get(user) + ts.getTime());
			} else {
				sumMap.put(user, ts.getTime());
			}
		}
		return sumMap;
	}
    
}
