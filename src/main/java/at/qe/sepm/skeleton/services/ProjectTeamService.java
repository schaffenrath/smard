package at.qe.sepm.skeleton.services;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.ProjectTeam;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.repositories.ProjectTeamRepository;
import at.qe.sepm.skeleton.utils.TeamOverviewData;

/**
 * Service for accessing ProjectTeam data.
 *
 * @author 
 */
@Component
@Scope("application")
public class ProjectTeamService {

    @Autowired
    private ProjectTeamRepository projectTeamRepository;
    
    @Autowired
    private UserService userService;

    @Autowired
    private WorkPackageService workPackageService;

    /**
     * Returns a collection of all Projects.
     *
     * @return
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    public List<ProjectTeam> getAllProjects() {
        return projectTeamRepository.findAll();
    }

    /**
     * Loads a single user identified by its username.
     *
     * @param username the username to search for
     * @return the user with the given username
     */
    @PreAuthorize("hasAuthority('MODERATOR')")
    public ProjectTeam loadProject(String projectTeamName) {
        return projectTeamRepository.findFirstByProjectTeamName(projectTeamName);
    }

    @PreAuthorize("hasAuthority('MODERATOR')")
    public ProjectTeam loadProjectId(Long projectTeamId) {
        return projectTeamRepository.findFirstByProjectTeamId(projectTeamId);
    }

    /**
     * Loads all ProjectTeams where the given user is moderator
     *
     * @param moderator as user
     * @return List of ProjectTeams
     */
    @PreAuthorize("hasAuthority('MODERATOR')")
    public List<ProjectTeam> loadAllProjects4Moderator(User moderator) {
        return projectTeamRepository.findAllByModerator(moderator);
    }
    
    /**
     * Get a Map, where all Workpackages for a given ProjectTeam are 
     * listed. In the values of the map, are the summed values of the
     * worked hours
     *
     * @param ProjectTeam
     * @return Map of WorkPackages with aggregated working hours
     */
    @PreAuthorize("hasAuthority('MODERATOR')")
    public Map<WorkPackage,TeamOverviewData> getOverview4ProjectTeam(ProjectTeam team){
    	Map<WorkPackage,TeamOverviewData> overview = new TreeMap<>();
    	for(User user : userService.getUser4ProjectTeam(team)){
    		overview = workPackageService.mergeOverview(overview, userService.getOverview4User(user), user);
    	}
    	return overview;
    }
    
    @PreAuthorize("hasAuthority('ADMIN')")
    public void deleteProjectTeam(ProjectTeam projectTeam) {
    	for (User user : userService.getUser4ProjectTeam(projectTeam)) {
    		userService.setProjectTeamToNull(user);
		}
    	projectTeamRepository.delete(projectTeam);
    }
    
    
    @PreAuthorize("hasAuthority('ADMIN')")
    public ProjectTeam saveProjectTeam(ProjectTeam projectTeam) {
        if (projectTeam.isNew()) {
            projectTeam.setCreateDate(new Date());
        }
        return projectTeamRepository.save(projectTeam);
    }
    
    @PreAuthorize("hasAuthority('ADMIN')")
    public void setModeratorToAdmin(User moderator) {
        for (ProjectTeam projectTeam : projectTeamRepository.findAllByModerator(moderator)) {
			projectTeam.setModerator(userService.loadUser("admin"));
			projectTeamRepository.save(projectTeam);
		}
    }
    
    @PreAuthorize("hasAuthority('MODERATOR')")
    public int getNumberOfMembers(ProjectTeam team) {
        return userService.getUser4ProjectTeam(team).size();
    }
    
    @PostConstruct
    public void init() {
    	userService.setProjectTeamService(this);
    }
    
    public Map<User, Double> getTotalTimesProject(ProjectTeam team){
    	final Map<User,Double> times = new TreeMap<>();
    	userService.getUser4ProjectTeam(team).forEach(new Consumer<User>() {
    		@Override
    		public void accept(User t) {
    			times.put(t, userService.getTotalTime(t));
    		}
    	
    	});
    	return times;
    }
    
}
