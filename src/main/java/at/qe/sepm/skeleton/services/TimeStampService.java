package at.qe.sepm.skeleton.services;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import at.qe.sepm.skeleton.model.TimeStamp;
import at.qe.sepm.skeleton.model.User;
import at.qe.sepm.skeleton.model.WorkPackage;
import at.qe.sepm.skeleton.repositories.TimeStampRepository;

/**
 * Service for accessing ProjectTeam data.
 *
 * @author
 */
@Component
@Scope("application")
public class TimeStampService {

    @Autowired
    private TimeStampRepository timeStampRepository;
    
    /**
     * Returns a collection of all Projects.
     *
     * @return
     */
    @PreAuthorize("hasAuthority('ADMIN')")
    public Collection<TimeStamp> getAllStamps() {
        return timeStampRepository.findAll();
    }

    /**
     * Loads a single user identified by its username.
     *
     * @param timeStampId
     * @return the user with the given username
     */
    @PreAuthorize("hasAuthority('ADMIN') or principal.username eq #username")
    public TimeStamp loadStamp(Long timeStampId) {
        return timeStampRepository.findFirstByTimeStampId(timeStampId);
    }

    public TimeStamp getOpenTimeStamp4User(User user){
    	return timeStampRepository.findFirstByUserAndTimeStampEndIsNull(user);
    }
    
    public List<TimeStamp> getClosedTimeStamps4User(User user){
    	return timeStampRepository.findAllByUserAndTimeStampEndIsNotNull(user);
    }
    
    public List<TimeStamp> findByUser(User user){
    	return timeStampRepository.findAllByUser(user);
    }

    public List<TimeStamp> getClosedTimeStamps4WorkPackage(WorkPackage wp){
    	return timeStampRepository.findAllByWorkPackageAndTimeStampEndIsNotNull(wp);
    }
    
    public void checkIn(WorkPackage workPackage, User user){
    	TimeStamp checkin = new TimeStamp();
    	checkin.setUser(user);
    	checkin.setWorkPackage(workPackage);
    	checkin.setTimeStampStart(new Date(System.currentTimeMillis()));
    	checkin.setCreateDate(checkin.getTimeStampStart());
    	timeStampRepository.save(checkin);
    }
    
    public void checkOut(TimeStamp timeStamp){
    	timeStamp.setTimeStampEnd(new Date(System.currentTimeMillis()));
    	timeStamp.calcTime();
    	timeStampRepository.save(timeStamp);
    }
    
    public void deleteByUser(User username){
    	for (TimeStamp timeStamp : timeStampRepository.findAllByUser(username)) {
    		timeStampRepository.delete(timeStamp);
		}
    	
    }
    public void setTimeStampToNull(WorkPackage workPackage){
    	for (TimeStamp timeStamp : timeStampRepository.findByWorkPackage(workPackage)) {
    		timeStamp.setWorkPackage(null);
    		timeStampRepository.save(timeStamp);
		}
    }
    
    public void deleteTimeStamp(TimeStamp timeStamp){
    	timeStampRepository.delete(timeStamp);
    }
    
}
