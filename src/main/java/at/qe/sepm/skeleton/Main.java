package at.qe.sepm.skeleton;

import at.qe.sepm.skeleton.configs.CustomServletContextInitializer;
import at.qe.sepm.skeleton.configs.WebSecurityConfig;
import at.qe.sepm.skeleton.utils.ViewScope;

import java.sql.SQLException;
import java.util.HashMap;
import javax.faces.webapp.FacesServlet;

import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

/**
 * Spring boot application. Execute maven with <code>mvn spring-boot:run</code>
 * to start this web application.
 *
 * @author Michael Brunner <Michael.Brunner@uibk.ac.at>
 */
@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class Main extends SpringBootServletInitializer {

	
    public static void main(String[] args) throws SQLException {
        SpringApplication.run(Main.class, args);
        //remove comments before both servers to enable sql debugging with localhost:8082 in jdbc:h2:mem:sepmskel
        //Server webServer = Server.createWebServer("-web","-webAllowOthers","-webPort","8082").start();
        //Server server = Server.createTcpServer("-tcp","-tcpAllowOthers","-tcpPort","9092").start();
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(new Class[]{Main.class, CustomServletContextInitializer.class, WebSecurityConfig.class});
    }

    @Bean
    public ServletRegistrationBean servletRegistrationBean() {
        FacesServlet servlet = new FacesServlet();
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(servlet, "*.xhtml");
        return servletRegistrationBean;
    }

    @Bean
    public CustomScopeConfigurer customScopeConfigurer() {
        CustomScopeConfigurer customScopeConfigurer = new CustomScopeConfigurer();
        HashMap<String, Object> customScopes = new HashMap<>();
        customScopes.put("view", new ViewScope());
        customScopeConfigurer.setScopes(customScopes);
        return customScopeConfigurer;
    }

}
